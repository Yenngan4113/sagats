import React, { useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import './App.css';
import cityApi from 'API/cityApi';
import studentApi from 'API/studentApi';
import { ListParams } from 'models';
import LoginPage from 'features/counter/auth/pages/LoginPage';
import { AdminLayout } from 'component/Layout';
import { NotFound, PrivateRoutes } from 'component/Common';
import { Button } from '@material-ui/core';
import { useAppDispatch } from 'app/hooks';
import { authAction } from 'features/counter/auth/authSlice';

function App() {
  const dispatch = useAppDispatch();
  const params: ListParams = {
    _page: 1,
    _limit: 10,
    _sort: 'name',
    _order: 'asc',
  };
  useEffect(() => {
    cityApi.getAll().then((res) => {
      res.data.map((item) => {});
    });
    studentApi.getAll(params).then((res) => {});
  }, []);
  const handleLogout = () => {
    dispatch(authAction.logout());
  };
  return (
    <div>
      <Button onClick={handleLogout}>Log out</Button>
      <Routes>
        <Route path="/login" element={<LoginPage />} />
        <Route element={<PrivateRoutes />}>
          <Route path="/admin" element={<AdminLayout />} />
        </Route>

        <Route path="/*" element={<NotFound />} />
      </Routes>
    </div>
  );
}

export default App;
