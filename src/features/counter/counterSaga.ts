import { PayloadAction } from '@reduxjs/toolkit';
import { delay, put, takeEvery, takeLatest } from 'redux-saga/effects';
import { increment, incrementSaga, incrementSagaSuccess } from './counterSlice';

export const log = (action: PayloadAction) => {
  console.log('Log', action);
};
function* handleIncrementSaga(action: PayloadAction<number>) {
  console.log('waiting 2s');
  // delay 2s
  yield delay(2000);
  console.log('waiting dispatch');
  yield put(incrementSagaSuccess(action.payload));
}
export default function* counterSaga() {
  console.log('counterSaga');
  yield takeLatest(incrementSaga.toString(), handleIncrementSaga);
}
