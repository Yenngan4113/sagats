import * as React from 'react';
import { Navigate, Outlet, Route, RouteProps } from 'react-router-dom';

// export interface PrivateRoute {}

export function PrivateRoutes(props: RouteProps) {
  const isLoggedIn = Boolean(localStorage.getItem('access_token'));
  //   if (!isLoggedIn) return <Route path="*" element={<Navigate to="/admin" replace />} />;
  return isLoggedIn ? <Outlet /> : <Navigate to="/login" />;
}
