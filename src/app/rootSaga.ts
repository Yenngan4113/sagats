import authSaga from 'features/counter/auth/authSaga';
import counterSaga from 'features/counter/counterSaga';
import { all } from 'redux-saga/effects';

// rootSaga với effect all là chạy tất cả các saga khác
export default function* rootSaga() {
  console.log('Root Saga');
  //   all là effect của Saga
  yield all([authSaga(), counterSaga()]);
}

function* helloSaga() {
  console.log('Hello saga');
}
